FROM php:8.1-apache

# Configura la timezone para México
RUN apt-get update && apt-get install -yq tzdata && ln -fs /usr/share/zoneinfo/America/Mexico_City /etc/localtime && dpkg-reconfigure -f noninteractive tzdata

# Configura el idioma Inglés para el contenedor
RUN apt update && apt install -y --no-install-recommends locales locales-all postgresql-client
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en  
ENV LC_ALL en_US.UTF-8 

# Instala las dependencias para PHP y Apache
RUN apt-get update && apt-get install -y apt-utils libpq-dev libxml2-dev git zip unzip libicu-dev libbz2-dev libpng-dev libjpeg-dev \
    libmcrypt-dev libreadline-dev libfreetype6-dev libzip-dev g++ imagemagick qpdf ; docker-php-ext-install soap pgsql pdo_pgsql gd intl mysqli pdo_mysql zip ; apt-get install -y libmagickwand-dev --no-install-recommends && pecl install imagick && docker-php-ext-enable imagick ; a2enmod rewrite ; curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
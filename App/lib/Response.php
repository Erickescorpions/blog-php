<?php
    namespace App\Lib;

    class Response
    {
        private $status = 200;

        public function status(int $code)
        {
            $this->status = $code;
            return $this;
        }
        
        public function toJSON($data = [])
        {
            http_response_code($this->status);
            // el header se usa para que se pueda enviar
            // el json al index
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

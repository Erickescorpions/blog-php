<?php
    namespace App\lib;
    
    class Router {
        public static function get($route, $callback) {
            if($_SERVER['REQUEST_METHOD'] == "GET") {
                Router::on($route, $callback);   
            }
        }

        public static function post($route, $callback) {
            if($_SERVER['REQUEST_METHOD'] == "POST") {
                Router::on($route, $callback);
            }
        }

        public static function on($route, $callback) {
            // obtiene la ruta del navegador a la que trataremos de acceder
            $params = $_SERVER['REQUEST_URI'];

            // por si acaso la ruta no viene con la raiz al inicio, se le pone
            $params = (stripos($params, "/") !== 0) ? "/" . $params : $params;

            // preparamos la ruta para usarla en la expresion regular
            $regex = str_replace('/', '\/', $route);

            // compara la ruta establecida y a la que se quiere acceder desde el navegador
            // y guarda las coincidencias en un array
            $is_match = preg_match('/^' . ($regex) . '$/', $params, $matches);

            if ($is_match) {
                // borramos la ruta ex "post/1" => "post"
                array_shift($matches);

                $callback(new Request($matches), new Response());
            }
        }

    }

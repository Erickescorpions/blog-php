<?php
    namespace App\lib;

    class Config {
        private static $config;

        public static function get($key, $default = null) {
            if(is_null(self::$config)) {
                // cargamos la configuracion a la clase
                self::$config = require_once(__DIR__ . '/../../config.php');
            }

            // si la variable que se solicita esta disponible se regresa
            // si no se devuelve el valor por default
            return !empty(self::$config[$key]) ? self::$config[$key] : $default; 
        }
    }
?>